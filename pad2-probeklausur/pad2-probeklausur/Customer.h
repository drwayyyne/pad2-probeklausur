#pragma once

#include <string>

class Customer {
public:
    Customer() :
    number{}, name{}, zip{}, city{}
    {
    }

    Customer(int no, std::string na, std::string z, std::string c) :
    number{no}, name{ na}, zip{ z}, city{ c}
    {
    }

    ~Customer() = default;

    int get_number() const {
        return number;
    }

    std::string get_name() const {
        return name;
    }

    std::string get_zip() const {
        return zip;
    }

    std::string get_city() const {
        return city;
    }

	friend std::ostream& operator<<(std::ostream&, const Customer&);
	friend bool operator==(const Customer&, const Customer&);

private:
    int number;
    std::string name;
    std::string zip;
    std::string city;
};

inline std::ostream& operator<<(std::ostream& os, const Customer& c) {
	return os << '#' << c.number << " "
			<< c.name << " "
			<< c.zip << " "
			<< c.city << '\n';
}

inline bool operator==(const Customer& a, const Customer& b) {
	if (a.number == b.number &&
		a.name == b.name &&
		a.zip == b.zip &&
		a.city == b.city)
		return true;
	else
		return false;
}