#pragma once

#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <numeric>
#include <vector>
#include "Customer.h"
#include "Purchase.h"

class Order {
public:

    Order() : cust(Customer{}), vP(std::vector<Purchase>{}) {
    }

    Order(Customer c, std::vector<Purchase> v) : cust{c}, vP{v}
    {
        std::sort(vP.begin(), vP.end());
    }
    
    ~Order() = default;

    Customer get_cust() const {return cust;}
    std::vector<Purchase> get_vP() {return vP;}
   

    friend bool operator<(const Order&, const Order&);
    friend std::ostream& operator<<(std::ostream&, const Order&);
    friend bool operator==(const Order&, const Order&);

private:
    Customer cust;
    std::vector<Purchase> vP;
};

inline bool operator<(const Order& a, const Order& b) {
	return a.cust.get_number() < b.cust.get_number();
}

inline std::ostream& operator<<(std::ostream& os, const Order& o) {
	
	os << o.cust;
	for (Purchase elem : o.vP)  os << elem; 
	return os;
}

inline bool operator==(const Order& a, const Order& b) {
	if (a.cust == b.cust && a.vP == b.vP)
		return true;
	return false;
}

inline void readFile(std::vector<Order>& vectOrd, const std::string filename) {

	std::ifstream is { filename.c_str() };
	std::istream_iterator<std::string> it { is };
	std::istream_iterator<std::string> eos{};

	while (it != eos) {

		std::string number{ *it++ };
		number.erase(number.begin());
		std::string name{ *it++ + " " + *it++ };
		std::string zip{ *it++ };
		std::string city{ *it++ };

		Customer newCust{ std::stoi(number), name, zip, city };
		std::vector<Purchase> vP{};

		while (it->at(0) != '#' && it != eos) {
			std::string productName{*it++};
			std::string unit_price{*it++};
			std::string count{*it++};
			vP.push_back(Purchase { productName, std::stod(unit_price), std::stoi(count) });
		}

		vectOrd.push_back(Order(newCust, vP));
	}
}

inline void writeFile(const std::vector<Order>& vectOrd, const std::string filename) {

	std::ofstream os{ filename.c_str() };
	

	for (std::vector<Order>::const_iterator it{ vectOrd.cbegin() }; it != vectOrd.cend(); ++it) {
		os << *it;
	}
}

template <typename T> inline void readFileT(T& containerT, const std::string filename) {

	std::ifstream is{ filename.c_str() };
	std::istream_iterator<std::string> it{is};
	std::istream_iterator<std::string> eos{};

	while (it != eos) {
		std::string number{ *it++ };
		number.erase(number.begin());
		std::string name{ *it++ + " " + *it++ };
		std::string zip{ *it++ };
		std::string city{ *it++ };

		Customer newCust{ std::stoi(number), name, zip, city };
		std::vector<Purchase> vP{};

		while (it->at(0) != '#' && it != eos) {
			std::string productName{ *it++ };
			std::string unit_price{ *it++ };
			std::string count{ *it++ };
			vP.push_back(Purchase{ productName, std::stod(unit_price), std::stoi(count) });
		}

		containerT.push_back(Order(newCust, vP));
	}
}

template <typename T> inline void writeFileT(const T& containerT, const std::string filename) {
	
	std::ofstream os{ filename.c_str() };
	if (!os) throw std::runtime_error{ "writeFileT(), file " + filename };
	for (typename T::const_iterator it = containerT.cbegin(); it != containerT.cend(); ++it) {
		os << *it;
	}
}

// ohne Duplikate = 12.418.755,94 - mit Duplikaten = 12.539.332,65