#pragma once

#include <string>

class Purchase {
private:
    std::string name;
    double unit_price;
    int count;

public:
    Purchase() : name{}, unit_price{}, count{}
    {
    }

    Purchase(std::string s, double d, int i) : name{s}, unit_price{d}, count{i}
    {
    }

    ~Purchase() = default;

    double get_unit_price() const {return unit_price;}
    int get_count() const { return count; }
    
    friend std::ostream& operator<<(std::ostream&, const Purchase&);
    friend bool operator<(const Purchase&, const Purchase&);
	friend bool operator==(const Purchase&, const Purchase&);


};

inline std::ostream& operator<<(std::ostream& os, const Purchase& p) {
	return os << p.name << " " << p.unit_price << " " << p.count << '\n';
}

inline bool operator<(const Purchase& a, const Purchase& b) {
    return a.name < b.name;
}

inline bool operator==(const Purchase& a, const Purchase& b) {
	if (a.name == b.name &&
		a.unit_price == b.unit_price &&
		a.count == b.count)
		return true;
	else
		return false;
}
