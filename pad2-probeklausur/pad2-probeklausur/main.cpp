/* 
 * File:   main.cpp
 * Author: ddegenha
 *
 * Created on 29. Dezember 2017, 17:18
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <list>
#include <numeric>
#include <stdexcept>
#include "Order.h"

using namespace std;

template <typename T> void printTestResult(bool successful, string testType, string descrption, T expected, T actual) {
	if (!successful) {
		cout << "FAILED TEST ";
	}
	else {
		cout << " passed test ";
	}
	cout << testType << '(' << descrption << ')';
	cout << ", expected: " << expected;
	cout << ", actual: " << actual << endl;
}

template <typename T> bool testEquals(const T exp, const T act, const string description, const bool silent = false) {
	bool result{ exp == act };

	if (!silent) {
		printTestResult(result, "testEquals", description, exp, act);
	}

	if (!result) throw runtime_error{ "Test failed" };

	return result;
}

/*
 * 
 */
int main(int argc, char** argv) try {

	std::vector<Order> vectOrd{};
	std::list<Order> listOrd{};

	readFileT(vectOrd, "cpo1.txt");
	readFileT(listOrd, "cpo2.txt");
	
	testEquals(int(11), int(vectOrd.size()), "Anzahl Elemente in Vector");
	testEquals(int(11), int(listOrd.size()), "Anzahl Elemente in List");

	testEquals(135791, vectOrd.at(5).get_cust().get_number(), "Kundennummer von Lewis Huey");
	testEquals(string("US-MT59870"), vectOrd.at(5).get_cust().get_zip(), "ZIP von Lewis Huey");
	testEquals(string("Stevensville"), vectOrd.at(5).get_cust().get_city(), "City von Lewis Huey");

	vector<Order> vMergedOrders{};
	vMergedOrders.resize(vectOrd.size() + listOrd.size());

	merge(vectOrd.begin(), vectOrd.end(), listOrd.begin(), listOrd.end(), vMergedOrders.begin());
        
    sort(vMergedOrders.begin(), vMergedOrders.end());

	//for (Order elem: vMergedOrders) cout << elem;
        
    double sum{};
    for (Order elem: vMergedOrders){
        for (Purchase p: elem.get_vP())
            sum = sum + p.get_unit_price() * p.get_count();
    }
    cout.precision(10);
    cout << sum << endl;
        
    vector<Order>::iterator it {unique(vMergedOrders.begin(), vMergedOrders.end())};
        
    vMergedOrders.resize(distance(vMergedOrders.begin(),it));
        
    sum = 0.0;
    for (Order elem : vMergedOrders) {
        for (Purchase p : elem.get_vP())
            sum = sum + p.get_unit_price() * p.get_count();
    }
    cout.precision(10);
    cout << sum;

	//for (Order elem : vectOrd) std::cout << elem;
	//for (Order elem : listOrd) std::cout << elem;

	//sort(vectOrd.begin(), vectOrd.end());
	//listOrd.sort();

	//for (Order elem : vectOrd) std::cout << elem;
	//for (Order elem : listOrd) std::cout << elem;

	//writeFileT(vectOrd, "sorted1.txt");
	//writeFileT(listOrd, "sorted2.txt");
    writeFileT(vMergedOrders, "sorted3.ansi");

    std::getchar();

    return 0;
}

catch (const std::exception& e) {
	cerr << "Error: " << e.what() << endl;
	return -2;
}
catch (...) {
	cerr << "Unknown Error" << endl;
	return -1;
}


// testEqual
// algorithm
// container
// mutators
// multiset
// sort
// find
// accumulate
// operator < 
// operator >>